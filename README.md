# DCS_paper 2022

## Description
Project to coordinate and make the distributed control systems final paper.

## Authors
Roberto Adamson Hernandez, Luca Antonelli, Sevrin Mathys

## Project status
Paper submitted

## Complexity graphs
The location of extra complexity plots is in sevy/images/*

## A guide to using the complexity data
The complexity data is contained within two exported matlab workspaces:
- dataset.mat contains most of the bulk data, including step counts and (terrible && useless) timing data.
- dataset_extension.mat contains neighbour data

The code used to make the figures in the paper is in dataset_proccessing.m

With matlab open, make sure to import both dataset files into the workspace before runnung any of the code to make figures, otherwise errors will happen.

