%% Initial definitions

% Define random vector x0
number_of_agents = 100;
steps = 1000;

x0 = randi([-50 50],number_of_agents,1); %generate set of initial conditions (agentsx1)
% Calculate final value as average of initial values
final_value = mean(x0);

X_averaging = zeros(number_of_agents, steps);
X_averaging(:,1) = x0; %substitute the first column of the zeros matrix for the initial conditions
X_accelerated_averaging = zeros(number_of_agents, steps);
X_accelerated_averaging(:,1) = x0;
% First call to accelerated averaging algorithm uses same x-value twice
x_minus_one = x0;

%% Nominal Case Example
% For now, assume sensors are in ring-like network with two neighbours each
% Create A as matrix with entries 1/3 on diagonal and both off-diagonals
A = diag(1/3*ones(number_of_agents,1));
for i=1:number_of_agents
    if i<number_of_agents
        A(i,i+1) = 1/3;
    end
    if i>1
        A(i,i-1) = 1/3;
    end
end
A(number_of_agents, 1) = 1/3;
A(1, number_of_agents) = 1/3;

%% 
beta = 1.4;
for i=2:steps
    X_averaging(:,i) = averaging(X_averaging(:,i-1),A); %employ simple update rule x(k+1) = Ax(k) for regular averaging, change columns 2 and ahead
    X_accelerated_averaging(:,i) = accelerated_averaging(X_accelerated_averaging(:,i-1), x_minus_one, A, beta); %employ update rule x(k+1) = beta*Ax(k) + (1-beta)*x(k-1) for accelerated averaging, change columns 2 and ahead
    x_minus_one = X_accelerated_averaging(:,i-1); %update the x(k-1) term with the actual value obtained in the matrix, which we will need for the next accelerated averaging iteration. We initially defined as x(0) for k=0 --> x(0)=x(-1) (random numbers generated at start of the code)
end
avg_error = calculate_error(X_averaging, final_value);
acc_avg_error = calculate_error(X_accelerated_averaging, final_value);

% Create plot with errors of both algorithms
t = linspace(1, steps, steps);

hold on;
plot(t, avg_error, 'DisplayName', 'Regular Averaging');
plot(t, acc_avg_error, 'DisplayName', 'Accelerated Averaging');
legend



%% Find assymetric matrix A1 --> directed graph G1
%using asymmetric pursuit matrix A from p.24
%convergent distributed averaging for k<1 
k = 0.5;
A1 = diag((1-k)*ones(number_of_agents,1));
for i=1:number_of_agents
    if i<number_of_agents
        A1(i,i+1) = k;
    end
    
end
A1(number_of_agents, 1) = k;

%A1 is asymmetric, row stochastic, and primitive ---> AA reaches avg
%consensus

%Gpursuit is SC, aperiodic and weight balanced
G1 = digraph(A1);


%% Find symmetric matrix A11 --> undirected graph G11
%using symmetric balancing matrix A from p.24
%convergent averaging for k<1/2

k = 1/2;
A11 = diag((1-2*k)*ones(number_of_agents,1));
for i=1:number_of_agents
    if i<number_of_agents
        A11(i,i+1) = k;
    end
    if i>1
        A11(i,i-1) = k;
    end
end
A11(number_of_agents, 1) = k;
A11(1, number_of_agents) = k;

%A11 is symmetric, row stochastic, and primitive ---> AA reaches avg
%consensus



%% use newly created A1 (asymmetric) on algorithms
beta = 0.6;
for i=2:steps
    X_averaging(:,i) = averaging(X_averaging(:,i-1),A1); %employ simple update rule x(k+1) = Ax(k) for regular averaging, change columns 2 and ahead
    X_accelerated_averaging(:,i) = accelerated_averaging(X_accelerated_averaging(:,i-1), x_minus_one, A1, beta); %employ update rule x(k+1) = beta*Ax(k) + (1-beta)*x(k-1) for accelerated averaging, change columns 2 and ahead
    x_minus_one = X_accelerated_averaging(:,i-1); %update the x(k-1) term with the actual value obtained in the matrix, which we will need for the next accelerated averaging iteration. We initially defined as x(0) for k=0 --> x(0)=x(-1) (random numbers generated at start of the code)
end
avg_error = calculate_error(X_averaging, final_value);
acc_avg_error = calculate_error(X_accelerated_averaging, final_value);

% Create plot with errors of both algorithms
t = linspace(1, steps, steps);

figure
hold on;
plot(t, avg_error, 'DisplayName', 'Regular Averaging');
plot(t, acc_avg_error, 'DisplayName', 'Accelerated Averaging');
legend(FontSize=10)
xlabel({'Number of Iterations'},FontSize=12);
ylabel('Average Error', FontSize=12)

% 
% figure
% plot(t, avg_error, 'DisplayName', 'Regular Averaging');
% legend

% AAA still converges for asymmetric matrix, AA converges to consensus.

%% use newly created A11 (symmetric) on algorithms
beta = 1.4;
for i=2:steps
    X_averaging(:,i) = averaging(X_averaging(:,i-1),A11); %employ simple update rule x(k+1) = Ax(k) for regular averaging, change columns 2 and ahead
    X_accelerated_averaging(:,i) = accelerated_averaging(X_accelerated_averaging(:,i-1), x_minus_one, A11, beta); %employ update rule x(k+1) = beta*Ax(k) + (1-beta)*x(k-1) for accelerated averaging, change columns 2 and ahead
    x_minus_one = X_accelerated_averaging(:,i-1); %update the x(k-1) term with the actual value obtained in the matrix, which we will need for the next accelerated averaging iteration. We initially defined as x(0) for k=0 --> x(0)=x(-1) (random numbers generated at start of the code)
end
avg_error = calculate_error(X_averaging, final_value);
acc_avg_error = calculate_error(X_accelerated_averaging, final_value);

% Create plot with errors of both algorithms
t = linspace(1, steps, steps);

figure
hold on;
plot(t, avg_error, 'DisplayName', 'Regular Averaging');
plot(t, acc_avg_error, 'DisplayName', 'Accelerated Averaging');
legend

% figure
% plot(t, avg_error, 'DisplayName', 'Regular Averaging');
% legend

% AAA converges more slowly than AA in this case, why? AA converges to consensus.

%% matrix Tb

T_b = [beta*A (1-beta)*eye(number_of_agents); eye(number_of_agents) zeros(number_of_agents)];
T_b*ones(2*number_of_agents,1) % check that T_b is row-stochastic
max(abs(eig(T_b))) % check that spectral radius of T_b is one

tf = issymmetric(T_b)

%check if matrix T_b is primitive 
n = number_of_agents;
n1 = n^2-2*n+2;tf
T1check = T_b^(n1);
check=all(T1check);
min(check) 

%matrix Tb is primitive

%% matrix Tb1 (Tb using asymmetric A1)
T_b1 = [beta*A1 (1-beta)*eye(number_of_agents); eye(number_of_agents) zeros(number_of_agents)];
T_b1*ones(2*number_of_agents,1) % check that T_b1 is row-stochastic for A1 = it is 
max(abs(eig(T_b1))) % check that spectral radius of T_b1 is one = it is not! this is why the AAA does not work for asymmetric matrices


%check if matrix T_b1 is primitive 
n = number_of_agents;
n1 = n^2-2*n+2;
T1check1 = T_b1^(n1);
check1=all(T1check1);
min(check1) % matrix is primitive if this equals 1 by Corollary 8.5.8 in Horn & Johnson, Matrix Analysis.


%T_b1 is primitive!

%% Matrix A2 that is not row-stochastic (using same matrix with a different constant k)
k2=1/4; %
A2 = diag(k2*ones(number_of_agents,1));
for i=1:number_of_agents
    if i<number_of_agents
        A2(i,i+1) = k2;
    end
    if i>1
        A2(i,i-1) = k2;
    end
end
A2(number_of_agents, 1) = k2;
A2(1, number_of_agents) = k2;

tf2 = issymmetric(A2); %check that A2 is symmetric = it is!

beta = 1.225;
for i=2:steps
    X_averaging(:,i) = averaging(X_averaging(:,i-1),A2); %employ simple update rule x(k+1) = Ax(k) for regular averaging, change columns 2 and ahead
    X_accelerated_averaging(:,i) = accelerated_averaging(X_accelerated_averaging(:,i-1), x_minus_one, A2, beta); %employ update rule x(k+1) = beta*Ax(k) + (1-beta)*x(k-1) for accelerated averaging, change columns 2 and ahead
    x_minus_one = X_accelerated_averaging(:,i-1); %update the x(k-1) term with the actual value obtained in the matrix, which we will need for the next accelerated averaging iteration. We initially defined as x(0) for k=0 --> x(0)=x(-1) (random numbers generated at start of the code)
end
avg_error = calculate_error(X_averaging, final_value);
acc_avg_error = calculate_error(X_accelerated_averaging, final_value);

% Create plot with errors of both algorithms
t = linspace(1, 50, 50);

figure
hold on;

plot(t, avg_error(1,1:50), 'DisplayName', 'Regular Averaging');

plot(t, acc_avg_error(1,1:50), 'DisplayName', 'Accelerated Averaging');
legend(FontSize=10)
xlabel({'Number of Iterations'},FontSize=12);
ylabel('Average Error', FontSize=12)


%result: neither AA nor AAA will converge for certain values of kappa
%(larger than 1/3) but they will converge for kappa<=1/3 *** 
% k=1/4;
%beta=1.225 then AAA is faster than AA

%% matrix Tb2 (Tb using A2)
T_b2 = [beta*A2 (1-beta)*eye(number_of_agents); eye(number_of_agents) zeros(number_of_agents)];
T_b2*ones(2*number_of_agents,1) % check that T_b2 is row-stochastic for A1 = it is NOT (k>1/3) 
max(abs(eig(T_b2))) % check that spectral radius of T_b2 is one = it is not! (k>1/3) this is why the AAA does not work for asymmetric matrices
max(abs(eig(A2)))

%check if matrix T_b2 is primitive 
n = number_of_agents;
n2 = n^2-2*n+2;
T2check2 = T_b2^(n2);
check2=all(T2check2);
min(check2) % matrix is primitive if this equals 1 by Corollary 8.5.8 in Horn & Johnson, Matrix Analysis.


%T_b2 is not primitive!

%% Matrix A3 that is not primitive but is symmetric and row-stochastic
%n must be even
k = 1/2;
A3 = diag(k*ones(number_of_agents,1));
for i=2:2:(number_of_agents-1)
    if i<number_of_agents
        A3(i,i+1) = k;
        A3(i+1,i) = k;
    end
   end
A3(number_of_agents, 1) = k;
A3(1, number_of_agents) = k;

n=number_of_agents;
n2 = n^2-2*n+2;
T3check = A3^(n2);
check3=all(T3check);
min(check3) % A3 is not primitive
issymmetric(A3) %A3 is symmetric
A3*ones(number_of_agents,1) %A3 is row-stochastic


beta = 1.6;
for i=2:steps
    X_averaging(:,i) = averaging(X_averaging(:,i-1),A3); %employ simple update rule x(k+1) = Ax(k) for regular averaging, change columns 2 and ahead
    X_accelerated_averaging(:,i) = accelerated_averaging(X_accelerated_averaging(:,i-1), x_minus_one, A3, beta); %employ update rule x(k+1) = beta*Ax(k) + (1-beta)*x(k-1) for accelerated averaging, change columns 2 and ahead
    x_minus_one = X_accelerated_averaging(:,i-1); %update the x(k-1) term with the actual value obtained in the matrix, which we will need for the next accelerated averaging iteration. We initially defined as x(0) for k=0 --> x(0)=x(-1) (random numbers generated at start of the code)
end
avg_error = calculate_error(X_averaging, final_value);
acc_avg_error = calculate_error(X_accelerated_averaging, final_value);

% Create plot with errors of both algorithms
t = linspace(1, 100, 100);

figure
hold on;
plot(t, avg_error(1,1:100), 'DisplayName', 'Regular Averaging');
plot(t, acc_avg_error(1,1:100), 'DisplayName', 'Accelerated Averaging');
legend(FontSize=10)
xlabel({'Number of Iterations'},FontSize=12);
ylabel('Average Error', FontSize=12)

% figure
% plot(t, avg_error, 'DisplayName', 'Regular Averaging');
% legend




%matrix Tb3 (Tb using A3)
T_b3 = [beta*A3 (1-beta)*eye(number_of_agents); eye(number_of_agents) zeros(number_of_agents)];
T_b3*ones(2*number_of_agents,1) % check that T_b2 is row-stochastic for A1 = it is NOT (k>1/3) 
sort(abs(eig(T_b3))) % check that spectral radius of T_b2 is one = it is not! (k>1/3) this is why the AAA does not work for asymmetric matrices
sort(abs(eig(A3))) % compare with the spectral radius of AA to see which one converges faster

%for this matrix A3, both AA and AAA converge for any k<1/2. AA is faster
%than AAA for k=1/2 and k<1/3, however, for 1/3<k<1/2, AAA converges faster
%for this specific matrix. 

%% declare functions

function [x_k] = accelerated_averaging(x_prev, x_prev_prev, A, beta)
    x_k = beta*A*x_prev + (1-beta)*x_prev_prev;
end


function [x_k]= averaging(x_prev, A)
    x_k = A*x_prev;
end

function [error] = calculate_error(X, average)
    [agents, steps] = size(X);
    error = zeros(1, steps);
    for i=1:steps
        for j=1:agents
            error(i) = error(i) + norm(X(j,i)-average)^2;
        end
    end
end
