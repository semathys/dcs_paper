n_values = 9;
run_count = 100;
powers = 2:n_values+1;
list_of_sizes = (2*ones(1, n_values)).^powers;

success = true;

for s=1:n_values
    N = list_of_sizes(s);
    disp(['starting runs for N==', num2str(N)])
    for r=1:run_count
        disp(['  starting run ', num2str(r), ' for N==', num2str(N)])
        e = randi([N+1, N*(N+1)/2]);
        
        counting = 0;
        connected = false;
        while ~connected
            disp(['    making graph for run ', num2str(r), ' attempt ', num2str(counting)]);
            Gtrue = graph(true(N)); % Self-loops are possible
            p = randperm(numedges(Gtrue), e);
            G = graph(Gtrue.Edges(p, :));
            if (numnodes(G) ~= numnodes(Gtrue))
                e = e+1;
                counting = counting + 1;
                connected = false;
                continue;
            end
            bins = conncomp(G);
            connected = all(bins == 1);
            e = e+1;
            counting = counting + 1;
        end
        
        adj = adjacency(G);
        deg = adj*ones(N,1);
        
        a = zeros(N,N);
        % Do non-diagonals:
        for i=1:N
            for j=1:N
                if (i~=j) && (adj(i,j)==1)
                    a(i,j) = 1/(1+max(deg(i),deg(j)));
                end
            end
        end
        % Do diagonals:
        for i=1:N
            colsum = 0;
            for h=1:N
                if (i~=h) && (adj(i,h)==1)
                    colsum = colsum + a(i,h);
                end
            end
            a(i,i) = 1-colsum;
        end
        % Check:
        lambdas = sort(eig(a,'vector'),'descend');
        rho_ess = eigs(a-ones(N,N)/N, 1, 'largestabs');
        if (norm(lambdas(1) - 1) >= 0.0001) || (lambdas(2) >= 1) || (lambdas(N) <= -1)  || (norm(a*ones(N,1) - ones(N,1)) >= 0.0001) || (norm(ones(N,1)'*a - ones(1,N)) >= 0.0001) || (norm(rho_ess - 1) <= 0.0001) || (numnodes(G) ~= numnodes(Gtrue))
            disp('problem!');
            disp(['following tripped:', num2str(norm(lambdas(1) - 1) >= 0.0001), num2str(lambdas(2) >= 1), num2str(lambdas(N) <= -1), num2str(norm(a*ones(N,1) - ones(N,1)) >= 0.0001), num2str(norm(ones(N,1)'*a - ones(1,N)) >= 0.0001), num2str(norm(rho_ess - 1) <= 0.0001), num2str(numnodes(G) ~= numnodes(Gtrue))]);
            full(a)
            full(adj)
            bins = conncomp(G)
            deg
            lambdas
            rho_ess
            plot(G)
            success = false;
            disp('dumped Info...');
        end
    end
end

success
