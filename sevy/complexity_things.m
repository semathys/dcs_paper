% setup statistics parameters:
n_values = 3; % note: don't go above 9, numeric instability (at least on my machine)
e_values = 5;
run_count = 10;
e = 0.001;  % (default) relative error, i.e. current error divided by initial error (percent away from average, where 100 percent is initial distance from average)
linf = 100000; % loop limit
verbose = true;  % if the script should notify

% setup the sizes:
two_powers = 2:n_values+1;
ten_powers = 1:e_values;
list_of_sizes = (2*ones(1, n_values)).^two_powers;
list_of_errors = (10*ones(1, e_values)).^(-ten_powers);
%list_of_sizes = [2048];


% setup the pre-compute storage:
A_storage = cell(e_values, n_values, run_count);
beta_star = zeros(e_values, n_values, run_count);
standard_essential_radius = zeros(e_values, n_values, run_count);
accel_essential_radius = zeros(e_values, n_values, run_count);
standard_convergence_step = zeros(e_values, n_values, run_count);
accel_convergence_step = zeros(e_values, n_values, run_count);
standard_c_epsilon = zeros(e_values, n_values, run_count);
accel_c_epsilon = zeros(e_values, n_values, run_count);
standard_steps_pre = zeros(e_values, n_values, run_count);
accel_steps_pre = zeros(e_values, n_values, run_count);
standard_upper_bound = zeros(e_values, n_values, run_count);
accel_upper_bound = zeros(e_values, n_values, run_count);
standard_nodal_bound = zeros(e_values, n_values, run_count);
accel_nodal_bound = zeros(e_values, n_values, run_count);

% setup the results storage:
standard_steps = zeros(e_values, n_values, run_count);
accel_steps = zeros(e_values, n_values, run_count);
accel_T_steps = zeros(e_values, n_values, run_count);
standard_tictock = zeros(e_values, n_values, run_count); % entire loop
standard_timings = zeros(e_values, n_values, run_count); % algorithm execution only
accel_tictock = zeros(e_values, n_values, run_count); % entire loop
accel_timings = zeros(e_values, n_values, run_count); % algorithm execution only
accel_T_tictock = zeros(e_values, n_values, run_count); % entire loop
accel_T_timings = zeros(e_values, n_values, run_count); % algorithm execution only
standard_vec_adds = zeros(e_values, n_values, run_count); % vector-adds
standard_mat_mult = zeros(e_values, n_values, run_count); % matrix-vector-mults
accel_vec_adds = zeros(e_values, n_values, run_count); % vector-adds
accel_mat_mult = zeros(e_values, n_values, run_count); % matrix-vector-mults
accel_T_vec_adds = zeros(e_values, n_values, run_count); % vector-adds
accel_T_mat_mult = zeros(e_values, n_values, run_count); % matrix-vector-mults

standard_max_nodal_adds = zeros(e_values, n_values, run_count); % adds
standard_max_nodal_mult = zeros(e_values, n_values, run_count); % mults
accel_max_nodal_adds = zeros(e_values, n_values, run_count); % adds
accel_max_nodal_mult = zeros(e_values, n_values, run_count); % mults
standard_pre_nodal_adds = zeros(e_values, n_values, run_count); % adds
standard_pre_nodal_mult = zeros(e_values, n_values, run_count); % mults
accel_pre_nodal_adds = zeros(e_values, n_values, run_count); % adds
accel_pre_nodal_mult = zeros(e_values, n_values, run_count); % mults

% Setup checking matrices:
check_infinities = zeros(e_values, n_values, run_count);
check_std_infinities = zeros(e_values, n_values, run_count);
check_accel_infinities = zeros(e_values, n_values, run_count);
check_T_infinities = zeros(e_values, n_values, run_count);
check_std_steps = zeros(e_values, n_values, run_count);
check_accel_steps = zeros(e_values, n_values, run_count);
check_T_steps = zeros(e_values, n_values, run_count);
check_beta_subtraction = zeros(e_values, n_values, run_count);
check_steps_upgrade = zeros(e_values, n_values, run_count);
check_T_sigma_max = zeros(e_values, n_values, run_count);

% Loop thru all error thresholds:
for t=1:e_values
    e = list_of_errors(t);
    if (verbose)
        disp(['starting computation for error==', num2str(e)])
    end

    % Loop thru the different N values:
    for s=1:n_values
        N=list_of_sizes(s);
        if (verbose)
            disp(['  starting runs for N==', num2str(N)])
        end
    
        % For every N value, do a bunch of runs:
        parfor r=1:run_count
            if (verbose)
                disp(['    starting run ', num2str(r), ' for N==', num2str(N)])
            end
    
            % first make an A for this run:
            A = create_A_matrix(N);
            A_storage{t,s,r} = A;
    
            % then get the required info to precalculate the bounds and
            % step-count for the standard algo:
            [V,D] = eig(A);
            temp_matrix = A-ones(N,N)/N;
            standard_essential_radius(t,s,r) = abs(eigs(temp_matrix, 1, 'largestabs'));
            standard_convergence_step(t,s,r) = standard_essential_radius(t,s,r);
    %         if isnan(standard_essential_radius(r, s))
    %             lambda = eig(temp_matrix);
    %             standard_essential_radius(r, s) = lambda(N,N);
    %         end
            standard_c_epsilon(t,s,r) = cond(V);
    
            % setup the accelerated algo:
            beta_star(t,s,r) = 2/(1+sqrt(1-standard_essential_radius(t,s,r)^2));
            T = [beta_star(t,s,r)*A, (1-beta_star(t,s,r))*eye(N); eye(N), zeros(N,N)];
    
            % get the required info to precalculate the bounds and
            % step-count for the accelerated algo:
            [U,G,W] = eig(T, 'vector');
            accel_essential_radius(t,s,r) = beta_star(t,s,r) - 1;
            accel_c_epsilon(t,s,r) = cond([V,zeros(size(V));zeros(size(V)),V]);
            %accel_essential_radius(t,s,r) = standard_essential_radius(t,s,r)/(1+sqrt(1-standard_essential_radius(t,s,r)^2));

            % trial and error:
            epsilon = (1-accel_essential_radius(t,s,r))/2;
            accel_c_epsilon(t,s,r) = cond(U)/epsilon;
    
            % pre-calculate upper bounds and step counts:
            standard_steps_pre(t,s,r) = ceil(log(e)/log(standard_convergence_step(t,s,r))); %ceil((log(e) - log(standard_c_epsilon(r,s)))/log(standard_essential_radius(r,s)+1));
            accel_steps_pre(t,s,r) = ceil((log(e)-log(accel_c_epsilon(t,s,r)))/(log(accel_essential_radius(t,s,r) + epsilon))); %ceil((log(e)-log(accel_c_epsilon(t,s,r)))/log(beta_star(t,s,r)-1));% ceil(log(e)/log(accel_convergence_step(r,s)+(1-accel_convergence_step(r,s))/2)); %ceil((log(e) - log(accel_c_epsilon(r,s)))/log(accel_essential_radius(r,s)+1));
            standard_upper_bound(t,s,r) = standard_steps_pre(t,s,r)*N*(2*N-1);
            accel_upper_bound(t,s,r) = accel_steps_pre(t,s,r)*2*N*(N+1);

            % get info about neighbors:
            max_neighbors = max(sum(A~=0))

            % nodal pre-complexity:
            standard_pre_nodal_adds(t,s,r) = (max_neighbors-1)*standard_steps_pre(t,s,r);
            standard_pre_nodal_mult(t,s,r) = max_neighbors*standard_steps_pre(t,s,r);
            accel_pre_nodal_adds(t,s,r) = (max_neighbors+1)*accel_steps_pre(t,s,r);
            accel_pre_nodal_mult(t,s,r) = max_neighbors*accel_steps_pre(t,s,r);
    
            % We start doing trials now:
            % setup initial x, final x and initial error:
            x_initial = randi([-50 50],N,1);
            x_final = mean(x_initial)*ones(N,1);
            e0 = norm(x_initial - x_final);
            % Ensure we don't start too close to the average
            while e0 < 10*e
                x_initial = randi([-50 50],N,1);
                x_final = mean(x_initial)*ones(N,1);
                e0 = norm(x_initial - x_final);
            end
    
            % do standard algo:
            stdStart = tic;
            infinity_breaker = 0;
            xnow = x_initial;
            error = norm(xnow-x_final)/e0;
            while error >= e
                % handle infinite loops:
                infinity_breaker = infinity_breaker + 1;
                if infinity_breaker > linf
                    check_std_infinities(t,s,r) = 1;
                    break
                end
    
                % do the update step (with timing):
                temp = tic;
                xnew = averaging(xnow, A);
                xnow = xnew;
                standard_timings(t,s,r) = standard_timings(t,s,r) + toc(temp);
    
                % count steps:
                standard_steps(t,s,r) = standard_steps(t,s,r) + 1;
    
                % keep track of operations done:
                standard_mat_mult(t,s,r) = standard_mat_mult(t,s,r)+1;

                % keep track of nodal operations:
                standard_max_nodal_adds(t,s,r) = standard_max_nodal_adds(t,s,r) + (max_neighbors-1);
                standard_max_nodal_mult(t,s,r) = standard_max_nodal_mult(t,s,r) + max_neighbors;
    
                % update error
                error = norm(xnow-x_final)/e0;
            end
            standard_tictock(t,s,r) = toc(stdStart);
    
    
            % do accelerated algo:
            accStart = tic;
            infinity_breaker = 0;
            xnow = x_initial;
            xold = x_initial;
            error = norm(xnow-x_final)/e0;
            while error >= e
                % handle infinite loops:
                infinity_breaker = infinity_breaker + 1;
                if infinity_breaker > linf
                    check_accel_infinities(t,s,r) = 1;
                    break
                end
    
                % do the update step (with timing):
                temp = tic;
                xnew = accelerated_averaging(xnow, xold, A, beta_star(t,s,r));
                xold = xnow;
                xnow = xnew;
                accel_timings(t,s,r) = accel_timings(t,s,r) + toc(temp);
    
                % count steps:
                accel_steps(t,s,r) = accel_steps(t,s,r) + 1;
    
                % keep track of operations done:
                accel_mat_mult(t,s,r) = accel_mat_mult(t,s,r)+1;
                accel_vec_adds(t,s,r) = accel_vec_adds(t,s,r)+1;

                % keep track of nodal operations:
                accel_max_nodal_adds(t,s,r) = accel_max_nodal_adds(t,s,r) + max_neighbors;
                accel_max_nodal_mult(t,s,r) = accel_max_nodal_mult(t,s,r) + (max_neighbors+1);
    
                % update error
                error = norm(xnow-x_final)/e0;
            end
            accel_tictock(t,s,r) = toc(accStart);
    
    
            % do T algo:
            actStart = tic;
            infinity_breaker = 0;
            xTnow = [x_initial;x_initial];
            error = norm(xTnow(1:N)-x_final)/e0;
            while error >= e
                % handle infinite loops:
                infinity_breaker = infinity_breaker + 1;
                if infinity_breaker > linf
                    check_T_infinities(t,s,r) = 1;
                    break
                end
    
                % do the update step (with timing):
                temp = tic;
                xTnew = averaging(xTnow, T);
                xTnow = xTnew;
                accel_T_timings(t,s,r) = accel_T_timings(t,s,r) + toc(temp);
    
                % count steps:
                accel_T_steps(t,s,r) = accel_T_steps(t,s,r) + 1;
    
                % keep track of operations done:
                accel_T_mat_mult(t,s,r) = accel_T_mat_mult(t,s,r)+4;
                %accel_T_vec_adds(t,s,r) = accel_T_vec_adds(t,s,r)+1;
    
                % update error
                error = norm(xTnow(1:N)-x_final)/e0;
            end
            accel_T_ticktoc(t,s,r) = toc(actStart);
    
            % Doing checks:
            check_std_steps(t,s,r) = standard_steps(t,s,r) <= standard_steps_pre(t,s,r);
            check_accel_steps(t,s,r) = accel_steps(t,s,r) <= accel_steps_pre(t,s,r);
            check_T_steps(t,s,r) = accel_T_steps(t,s,r) <= accel_steps_pre(t,s,r);
            check_beta_subtraction(t,s,r) = beta_star(t,s,r) - 1 > 0;
            check_steps_upgrade(t,s,r) = accel_steps(t,s,r) <= standard_steps(t,s,r);
            check_T_sigma_max(t,s,r) = svds(T, 1, 'largest');
            check_infinities(t,s,r) = check_std_infinities(t,s,r) || check_accel_infinities(t,s,r) || check_T_infinities(t,s,r)
    
        end
    end
end

if (verbose)
    disp('Jobs Done!');
end

if (verbose)
    disp('Doing some checks: (one is check passed, 0 means there was an anomaly)');
    disp(['pre-steps (standard) is upper-bounded:', num2str(all(check_std_steps, 'all'))]);
    disp(['pre-steps (accel) is upper-bounded:', num2str(all(check_accel_steps, 'all'))]);
    disp(['pre-steps (T) is upper-bounded:', num2str(all(check_T_steps, 'all'))]);
    disp(['accel is better:', num2str(all(check_steps_upgrade, 'all'))]);
    disp(['beta-1 is positive:', num2str(all(check_beta_subtraction, 'all'))]);
    disp(['no infinties:', num2str(~any(check_infinities, 'all'))]);
    bigness = size(check_steps_upgrade);
    if ~all(check_steps_upgrade, 'all')
        for k=1:bigness(1)
            for i=1:bigness(2)
                counting = 0;
                for j=1:bigness(3)
                    if check_steps_upgrade(k,i,j) == 0
                        counting = counting + 1;
                        disp(['Found an anomaly (accel is worse).  N == ', num2str(2^(i+1)), ', Run: ', num2str(j), ', Error: ', num2str(10^(-k)), ' | step disparity: ', num2str(accel_steps(k,i,j)), ' - ', num2str(standard_steps(k,i,j)), ' == ', num2str(accel_steps(k,i,j) - standard_steps(k,i,j)), ' | The Rhos:  std < ', num2str(standard_essential_radius(k,i,j)),' >  accel < ', num2str(accel_essential_radius(k,i,j)),' >']);
                    end
                end
                if counting > 0
                    disp(['Total (accel is worse) anomalies for N == ', num2str(2^(i+1)), ' and Error == ', num2str(10^(-k)), ' : ' num2str(counting)]);
                end
            end
        end
    end
    if ~all(check_std_steps, 'all')
        for k=1:bigness(1)
            for i=1:bigness(2)
                counting = 0;
                for j=1:bigness(3)
                    if check_std_steps(k,i,j) == 0
                        counting = counting + 1;
                        disp(['Found an anomaly (std theoretical is worse).  N == ', num2str(2^(i+1)), ', Run: ', num2str(j), ', Error: ', num2str(10^(-k)), ' | step disparity: ', num2str(standard_steps_pre(k,i,j)), ' - ', num2str(standard_steps(k,i,j)), ' == ', num2str(standard_steps_pre(k,i,j) - standard_steps(k,i,j)), ' | The Rhos:  std < ', num2str(standard_essential_radius(k,i,j)),' >  accel < ', num2str(accel_essential_radius(k,i,j)),' >']);
                    end
                end
                if counting > 0
                    disp(['Total (std theoretical is worse) anomalies for N == ', num2str(2^(i+1)), ' and Error == ', num2str(10^(-k)), ' : ' num2str(counting)]);
                end
            end
        end
    end
    if ~all(check_accel_steps, 'all')
        for k=1:bigness(1)
            for i=1:bigness(2)
                counting = 0;
                for j=1:bigness(3)
                    if check_accel_steps(k,i,j) == 0
                        counting = counting + 1;
                        disp(['Found an anomaly (accel theoretical is worse).  N == ', num2str(2^(i+1)), ', Run: ', num2str(j), ', Error: ', num2str(10^(-k)), ' | step disparity: ', num2str(accel_steps_pre(k,i,j)), ' - ', num2str(accel_steps(k,i,j)), ' == ', num2str(accel_steps_pre(k,i,j) - accel_steps(k,i,j)), ' | The Rhos:  std < ', num2str(standard_essential_radius(k,i,j)),' >  accel < ', num2str(accel_essential_radius(k,i,j)),' >']);
                    end
                end
                if counting > 0
                    disp(['Total (accel theoretical is worse) anomalies for N == ', num2str(2^(i+1)), ' and Error == ', num2str(10^(-k)), ' : ' num2str(counting)]);
                end
            end
        end
    end
    if any(check_infinities, 'all')
        for k=1:bigness(1)
            for i=1:bigness(2)
                counting = 0;
                for j=1:bigness(3)
                    if check_std_infinities(k,i,j) == 1
                        counting = counting + 1;
                        disp(['Found an anomaly (std broke infinity).  N == ', num2str(2^(i+1)), ', Run: ', num2str(j), ', Error: ', num2str(10^(-k)), ' | The Rhos:  std < ', num2str(standard_essential_radius(k,i,j)),' >  accel < ', num2str(accel_essential_radius(k,i,j)),' >']);
                    end
                    if check_accel_infinities(k,i,j) == 1
                        counting = counting + 1;
                        disp(['Found an anomaly (accel broke infinity).  N == ', num2str(2^(i+1)), ', Run: ', num2str(j), ', Error: ', num2str(10^(-k)), ' | The Rhos:  std < ', num2str(standard_essential_radius(k,i,j)),' >  accel < ', num2str(accel_essential_radius(k,i,j)),' >']);
                    end
                    if check_T_infinities(k,i,j) == 1
                        counting = counting + 1;
                        disp(['Found an anomaly (T broke infinity).  N == ', num2str(2^(i+1)), ', Run: ', num2str(j), ', Error: ', num2str(10^(-k)), ' | The Rhos:  std < ', num2str(standard_essential_radius(k,i,j)),' >  accel < ', num2str(accel_essential_radius(k,i,j)),' >']);
                    end
                end
                if counting > 0
                    disp(['Total (broke infinity) anomalies for N == ', num2str(2^(i+1)), ' and Error == ', num2str(10^(-k)), ' : ' num2str(counting)]);
                end
            end
        end
    end
end
