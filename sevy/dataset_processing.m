% x=2:12;
% y=2.^x;
% be = (y-ones(size(y)))./(y+ones(size(y)));
% figure();
% hold on;
% semilogx(x,dataset_avg_std_steps(:, 4), 'DisplayName', 'average standard steps');
% semilogx(x,dataset_avg_acc_steps(:, 4), 'DisplayName', 'average accelerated steps');
% %semilogx(x,be(:), 'DisplayName', '% required for FLOPs efficiency');
% xlabel ('log2(N)')
% ylabel('avg # of steps for e = 0.0001')
% legend('Location','northeast')
% exportgraphics(gcf,'dcs_paper/sevy/images/avg_steps_comparisson.eps');


% x=2:12;
% 
% figure();
% hold on;
% semilogx(x,dataset_max_std_steps(:, 4), 'DisplayName', 'maximum standard steps');
% semilogx(x,dataset_max_std_steps_pre(:, 4), 'DisplayName', 'maximum precomputed standard steps');
% xlabel ('log2(N)')
% ylabel('max # of steps for e = 0.0001')
% legend('Location','southwest')
% exportgraphics(gcf,'dcs_paper/sevy/images/standard_max_steps_is_upperbounded.eps');
% 
% figure();
% hold on;
% semilogx(x,dataset_avg_std_steps(:, 4), 'DisplayName', 'average standard steps');
% semilogx(x,dataset_avg_std_steps_pre(:, 4), 'DisplayName', 'average precomputed standard steps');
% xlabel ('log2(N)')
% ylabel('avg # of steps for e = 0.0001')
% legend('Location','southwest')
% exportgraphics(gcf,'dcs_paper/sevy/images/standard_avg_steps_is_upperbounded.eps');
% 
% 
% figure();
% hold on;
% semilogx(x,dataset_max_acc_steps(:, 4), 'DisplayName', 'maximum accelerated steps');
% semilogx(x,dataset_max_acc_steps_pre(:, 4), 'DisplayName', 'maximum precomputed accelerated steps');
% xlabel ('log2(N)')
% ylabel('max # of steps for e = 0.0001')
% legend('Location','southwest')
% exportgraphics(gcf,'dcs_paper/sevy/images/accel_max_steps_is_upperbounded.eps');
% 
% figure();
% hold on;
% semilogx(x,dataset_avg_acc_steps(:, 4), 'DisplayName', 'average accelerated steps');
% semilogx(x,dataset_avg_acc_steps_pre(:, 4), 'DisplayName', 'average precomputed accelerated steps');
% xlabel ('log2(N)')
% ylabel('avg # of steps for e = 0.0001')
% legend('Location','southwest')
% exportgraphics(gcf,'dcs_paper/sevy/images/accel_avg_steps_is_upperbounded.eps');
% 
% figure();
% hold on;
% for i=1:11
%     if i==10 || i==11
%         semilogx(dataset_avg_acc_steps(i, 1:5), 'DisplayName',['N==' num2str(dataset_sizes_axis(i))]);
%         if i==10
%             text(5.01,dataset_avg_acc_steps(i, 5)+0.2,num2str(dataset_sizes_axis(i)))
%         elseif i==11
%             text(5.01,dataset_avg_acc_steps(i, 5),num2str(dataset_sizes_axis(i)))
%         end
%         continue
%     end
%     semilogx(dataset_avg_acc_steps(i, :), 'DisplayName',['N==' num2str(dataset_sizes_axis(i))]);
%     text(6.01,dataset_avg_acc_steps(i, 6),num2str(dataset_sizes_axis(i)))
% end
% xticks(1:6)
% xlabel ('-log10(e)')
% ylabel('avg # of steps')
% exportgraphics(gcf,'dcs_paper/sevy/images/accel_avg_steps_increases_with_e.eps');
% 
% 
% figure();
% hold on;
% semilogx(x,dataset_max_std_steps(:, 1), 'DisplayName', 'maximum standard steps');
% semilogx(x,dataset_max_std_steps_pre(:, 1), 'DisplayName', 'maximum precomputed standard steps');
% xlabel ('log2(N)');
% ylabel('max # of steps for e = 0.1');
% legend('Location','southwest');
% exportgraphics(gcf,'dcs_paper/sevy/images/standard_max_steps_is_upperbounded_1.eps');
% 
% figure();
% hold on;
% semilogx(x,dataset_avg_std_steps(:, 1), 'DisplayName', 'average standard steps');
% semilogx(x,dataset_avg_std_steps_pre(:, 1), 'DisplayName', 'average precomputed standard steps');
% xlabel ('log2(N)');
% ylabel('avg # of steps for e = 0.1');
% legend('Location','southwest');
% exportgraphics(gcf,'dcs_paper/sevy/images/standard_avg_steps_is_upperbounded_1.eps');
% 
% figure();
% hold on;
% semilogx(x,dataset_max_acc_steps(:, 1), 'DisplayName', 'maximum accelerated steps');
% semilogx(x,dataset_max_acc_steps_pre(:, 1), 'DisplayName', 'maximum precomputed accelerated steps');
% xlabel ('log2(N)');
% ylabel('max # of steps for e = 0.1');
% legend('Location','southwest');
% exportgraphics(gcf,'dcs_paper/sevy/images/accel_max_steps_is_upperbounded_1.eps');
% 
% figure();
% hold on;
% semilogx(x,dataset_avg_acc_steps(:, 1), 'DisplayName', 'average accelerated steps');
% semilogx(x,dataset_avg_acc_steps_pre(:, 1), 'DisplayName', 'average precomputed accelerated steps');
% xlabel ('log2(N)');
% ylabel('avg # of steps for e = 0.1');
% legend('Location','southwest');
% exportgraphics(gcf,'dcs_paper/sevy/images/accel_avg_steps_is_upperbounded_1.eps');
% 
% 
% 
% figure();
% hold on;
% semilogx(x,dataset_max_std_steps(:, 2), 'DisplayName', 'maximum standard steps');
% semilogx(x,dataset_max_std_steps_pre(:, 2), 'DisplayName', 'maximum precomputed standard steps');
% xlabel ('log2(N)');
% ylabel('max # of steps for e = 0.01');
% legend('Location','southwest');
% exportgraphics(gcf,'dcs_paper/sevy/images/standard_max_steps_is_upperbounded_2.eps');
% 
% figure();
% hold on;
% semilogx(x,dataset_avg_std_steps(:, 2), 'DisplayName', 'average standard steps');
% semilogx(x,dataset_avg_std_steps_pre(:, 2), 'DisplayName', 'average precomputed standard steps');
% xlabel ('log2(N)');
% ylabel('avg # of steps for e = 0.01');
% legend('Location','southwest');
% exportgraphics(gcf,'dcs_paper/sevy/images/standard_avg_steps_is_upperbounded_2.eps');
% 
% figure();
% hold on;
% semilogx(x,dataset_max_acc_steps(:, 2), 'DisplayName', 'maximum accelerated steps');
% semilogx(x,dataset_max_acc_steps_pre(:, 2), 'DisplayName', 'maximum precomputed accelerated steps');
% xlabel ('log2(N)');
% ylabel('max # of steps for e = 0.01');
% legend('Location','southwest');
% exportgraphics(gcf,'dcs_paper/sevy/images/accel_max_steps_is_upperbounded_2.eps');
% 
% figure();
% hold on;
% semilogx(x,dataset_avg_acc_steps(:, 2), 'DisplayName', 'average accelerated steps');
% semilogx(x,dataset_avg_acc_steps_pre(:, 2), 'DisplayName', 'average precomputed accelerated steps');
% xlabel ('log2(N)');
% ylabel('avg # of steps for e = 0.01');
% legend('Location','southwest');
% exportgraphics(gcf,'dcs_paper/sevy/images/accel_avg_steps_is_upperbounded_2.eps');
% 
% figure();
% hold on;
% semilogx(x,dataset_max_std_steps(:, 3), 'DisplayName', 'maximum standard steps');
% semilogx(x,dataset_max_std_steps_pre(:, 3), 'DisplayName', 'maximum precomputed standard steps');
% xlabel ('log2(N)');
% ylabel('max # of steps for e = 0.001');
% legend('Location','southwest');
% exportgraphics(gcf,'dcs_paper/sevy/images/standard_max_steps_is_upperbounded_3.eps');
% 
% figure();
% hold on;
% semilogx(x,dataset_avg_std_steps(:, 3), 'DisplayName', 'average standard steps');
% semilogx(x,dataset_avg_std_steps_pre(:, 3), 'DisplayName', 'average precomputed standard steps');
% xlabel ('log2(N)');
% ylabel('avg # of steps for e = 0.001');
% legend('Location','southwest');
% exportgraphics(gcf,'dcs_paper/sevy/images/standard_avg_steps_is_upperbounded_3.eps');
% 
% figure();
% hold on;
% semilogx(x,dataset_max_acc_steps(:, 3), 'DisplayName', 'maximum accelerated steps');
% semilogx(x,dataset_max_acc_steps_pre(:, 3), 'DisplayName', 'maximum precomputed accelerated steps');
% xlabel ('log2(N)');
% ylabel('max # of steps for e = 0.001');
% legend('Location','southwest');
% exportgraphics(gcf,'dcs_paper/sevy/images/accel_max_steps_is_upperbounded_3.eps');
% 
% figure();
% hold on;
% semilogx(x,dataset_avg_acc_steps(:, 3), 'DisplayName', 'average accelerated steps');
% semilogx(x,dataset_avg_acc_steps_pre(:, 3), 'DisplayName', 'average precomputed accelerated steps');
% xlabel ('log2(N)');
% ylabel('avg # of steps for e = 0.001');
% legend('Location','southwest');
% exportgraphics(gcf,'dcs_paper/sevy/images/accel_avg_steps_is_upperbounded_3.eps');
% 
% figure();
% hold on;
% semilogx(x,dataset_max_std_steps(:, 5), 'DisplayName', 'maximum standard steps');
% semilogx(x,dataset_max_std_steps_pre(:, 5), 'DisplayName', 'maximum precomputed standard steps');
% xlabel ('log2(N)');
% ylabel('max # of steps for e = 0.00001');
% legend('Location','southwest');
% exportgraphics(gcf,'dcs_paper/sevy/images/standard_max_steps_is_upperbounded_5.eps');
% 
% figure();
% hold on;
% semilogx(x,dataset_avg_std_steps(:, 5), 'DisplayName', 'average standard steps');
% semilogx(x,dataset_avg_std_steps_pre(:, 5), 'DisplayName', 'average precomputed standard steps');
% xlabel ('log2(N)');
% ylabel('avg # of steps for e = 0.00001');
% legend('Location','southwest');
% exportgraphics(gcf,'dcs_paper/sevy/images/standard_avg_steps_is_upperbounded_5.eps');
% 
% figure();
% hold on;
% semilogx(x,dataset_max_acc_steps(:, 5), 'DisplayName', 'maximum accelerated steps');
% semilogx(x,dataset_max_acc_steps_pre(:, 5), 'DisplayName', 'maximum precomputed accelerated steps');
% xlabel ('log2(N)');
% ylabel('max # of steps for e = 0.00001');
% legend('Location','southwest');
% exportgraphics(gcf,'dcs_paper/sevy/images/accel_max_steps_is_upperbounded_5.eps');
% 
% figure();
% hold on;
% semilogx(x,dataset_avg_acc_steps(:, 5), 'DisplayName', 'average accelerated steps');
% semilogx(x,dataset_avg_acc_steps_pre(:, 5), 'DisplayName', 'average precomputed accelerated steps');
% xlabel ('log2(N)');
% ylabel('avg # of steps for e = 0.00001');
% legend('Location','southwest');
% exportgraphics(gcf,'dcs_paper/sevy/images/accel_avg_steps_is_upperbounded_5.eps');
% 
% x=2:10;
% figure();
% hold on;
% semilogx(x,dataset_max_std_steps(1:9, 6), 'DisplayName', 'maximum standard steps');
% semilogx(x,dataset_max_std_steps_pre(1:9, 6), 'DisplayName', 'maximum precomputed standard steps');
% xlabel ('log2(N)')
% ylabel('max # of steps for e = 0.000001')
% legend('Location','southwest')
% exportgraphics(gcf,'dcs_paper/sevy/images/standard_max_steps_is_upperbounded_6.eps');
% 
% figure();
% hold on;
% semilogx(x,dataset_avg_std_steps(1:9, 6), 'DisplayName', 'average standard steps');
% semilogx(x,dataset_avg_std_steps_pre(1:9, 6), 'DisplayName', 'average precomputed standard steps');
% xlabel ('log2(N)')
% ylabel('avg # of steps for e = 0.000001')
% legend('Location','southwest')
% exportgraphics(gcf,'dcs_paper/sevy/images/standard_avg_steps_is_upperbounded_6.eps');
% 
% figure();
% hold on;
% semilogx(x,dataset_max_acc_steps(1:9, 6), 'DisplayName', 'maximum accelerated steps');
% semilogx(x,dataset_max_acc_steps_pre(1:9, 6), 'DisplayName', 'maximum precomputed accelerated steps');
% xlabel ('log2(N)')
% ylabel('max # of steps for e = 0.000001')
% legend('Location','southwest')
% exportgraphics(gcf,'dcs_paper/sevy/images/accel_max_steps_is_upperbounded_6.eps');
% 
% figure();
% hold on;
% semilogx(x,dataset_avg_acc_steps(1:9, 6), 'DisplayName', 'average accelerated steps');
% semilogx(x,dataset_avg_acc_steps_pre(1:9, 6), 'DisplayName', 'average precomputed accelerated steps');
% xlabel ('log2(N)')
% ylabel('avg # of steps for e = 0.000001')
% legend('Location','southwest')
% exportgraphics(gcf,'dcs_paper/sevy/images/accel_avg_steps_is_upperbounded_6.eps');
% 
% 
% 
% 
% 
% 
% 
% 
% max_neighbors_4_512 = dataset_expansion_max_neighbors(1:8,:,:);
% max_neighbors_1024 = dataset_expansion_max_neighbors(9,:,1:500);
% max_neighbors_2048_4096 = dataset_expansion_max_neighbors(10:11,1:5,1:10);
% 
% average_max_neighbors_512 = mean(max_neighbors_4_512,3);
% average_max_neighbors_1024 = mean(max_neighbors_1024,3);
% average_max_neighbors_2048 = mean(max_neighbors_2048_4096,3);
% average_max_neighbors = [average_max_neighbors_512; average_max_neighbors_1024; average_max_neighbors_2048, [0;0]];
% 
% maximum_max_neighbors_512 = max(max_neighbors_4_512,[],3);
% maximum_max_neighbors_1024 = max(max_neighbors_1024,[],3);
% maximum_max_neighbors_2048 = max(max_neighbors_2048_4096,[],3);
% maximum_max_neighbors = [maximum_max_neighbors_512; maximum_max_neighbors_1024; maximum_max_neighbors_2048, [0;0]];
% 
% nodal_flops_std_max = (2*maximum_max_neighbors - ones(size(maximum_max_neighbors))).*dataset_max_std_steps;
% nodal_flops_acc_max = (2*maximum_max_neighbors + ones(size(maximum_max_neighbors))).*dataset_max_acc_steps;
% nodal_flops_std_avg = (2*average_max_neighbors - ones(size(average_max_neighbors))).*dataset_avg_std_steps;
% nodal_flops_acc_avg = (2*average_max_neighbors + ones(size(average_max_neighbors))).*dataset_avg_acc_steps;
% 
% nodal_flops_std_max_pre = (2*maximum_max_neighbors - ones(size(maximum_max_neighbors))).*dataset_max_std_steps_pre;
% nodal_flops_acc_max_pre = (2*maximum_max_neighbors + ones(size(maximum_max_neighbors))).*dataset_max_acc_steps_pre;
% nodal_flops_std_avg_pre = (2*average_max_neighbors - ones(size(average_max_neighbors))).*dataset_avg_std_steps_pre;
% nodal_flops_acc_avg_pre = (2*average_max_neighbors + ones(size(average_max_neighbors))).*dataset_avg_acc_steps_pre;
% 
% x=2:12;
% figure();
% hold on;
% semilogx(x,nodal_flops_std_max(:, 4), 'DisplayName', 'maximum standard nodal FLOPs');
% semilogx(x,nodal_flops_acc_max(:, 4), 'DisplayName', 'maximum accelerated nodal FLOPs');
% xlabel ('log2(N)')
% ylabel('max FLOPs for e = 0.0001')
% legend('Location','northwest')
% exportgraphics(gcf,'dcs_paper/sevy/images/nodal_flops_max_comparisson.eps');
% 
% x=2:12;
% figure();
% hold on;
% semilogx(x,nodal_flops_std_avg(:, 4)./nodal_flops_std_avg(:, 4), 'DisplayName', '% average standard nodal FLOPs');
% semilogx(x,nodal_flops_acc_avg(:, 4)./nodal_flops_std_avg(:, 4), 'DisplayName', '% average accelerated nodal FLOPs');
% xlabel ('log2(N)')
% ylabel('avg FLOPs ratio for e = 0.0001')
% legend('Location','southeast')
% exportgraphics(gcf,'dcs_paper/sevy/images/nodal_flops_avg_comparisson.eps');
%
% x=2:12;
% figure();
% hold on;
% semilogx(x,log2(maximum_max_neighbors(:, 4)), 'DisplayName', 'maximum neighbors');
% semilogx(x,log2(average_max_neighbors(:, 4)), 'DisplayName', 'average neighbors');
% xlabel ('log2(N)')
% ylabel('log2(# of neighbors) for e = 0.0001')
% legend('Location','northwest')
% exportgraphics(gcf,'dcs_paper/sevy/images/nodal_neighbors_comparisson.eps');

% y = dataset_max_std_steps.*(2*maximum_max_neighbors - ones(size(maximum_max_neighbors)))./(2*maximum_max_neighbors + ones(size(maximum_max_neighbors)));
% figure();
% hold on;
% semilogx(x,y(:, 4), 'DisplayName', 'adjusted maximum standard steps');
% semilogx(x,dataset_max_acc_steps(:, 4), 'DisplayName', 'maximum accelerated steps');
% xlabel ('log2(N)')
% ylabel('adjusted steps for e = 0.0001')
% legend('Location','northeast')
% exportgraphics(gcf,'dcs_paper/sevy/images/nodal_break_even_comparisson.eps');