function [A] = create_A_matrix(Number)
%CREATE_A_MATRIX Creates A matrix with size NxN
%   Takes an int N and creates a (random) symmetric, stocastic, A suitable
%   for the Accelerated Averageing Algorithm.
    N = Number;
    failed = true;
    while failed
        failed = false;
        % define a random amount of edges
        e = randi([N+1, N*(N+1)/2-1]);
        connected = false;
        while ~connected
            % Make a random graph.  if it's not connected, try again with an
            % aditional edge.  continue until the graph is connected.
            Gtrue = graph(true(N)); 
            p = randperm(numedges(Gtrue), e);
            G = graph(Gtrue.Edges(p, :));
            if (numnodes(G) ~= numnodes(Gtrue)) % ensure we still have N nodes
                e = e+1;
                connected = false;
                continue;
            end
            bins = conncomp(G);
            connected = all(bins == 1);
            e = e+1;
        end
        
        % get info about the adjacency and degree of each node
        adj = adjacency(G);
        deg = adj*ones(N,1);
        
        % begin building our A matrix (using the The Metropolis–Hastings
        % method, see section 5.3.2 of the lecture notes for details)
    
        a = zeros(N,N);
        % Do non-diagonals:
        for i=1:N
            for j=1:N
                if (i~=j) && (adj(i,j)==1)
                    a(i,j) = 1/(1+max(deg(i),deg(j)));
                end
            end
        end
        % Do diagonals:
        for i=1:N
            colsum = 0;
            for h=1:N
                if (i~=h) && (adj(i,h)==1)
                    colsum = colsum + a(i,h);
                end
            end
            a(i,i) = 1-colsum;
        end
    end

    % Return the A
    A=a;
end

