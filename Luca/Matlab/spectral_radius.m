function [rho] = spectral_radius(eigenvalues)
    filter = abs(eigenvalues - 1) > 0.01;
    rho = max(abs(eigenvalues(filter)));
end