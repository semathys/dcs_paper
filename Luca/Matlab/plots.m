x = readtable('comparison.txt', 'VariableNamingRule','preserve');
rhos = x(:,'rho_ess');
algorithms = x(:,"Alg.");
str_algorithms = string(table2array(algorithms));
boxplot(table2array(rhos), str_algorithms)
xticklabels({'Algorithm 1', 'Algorithm 2', 'Algorithm 3', 'Algorithm 4', 'Algorithm 5', 'Distr. Averaging', 'Acc. Averaging'});
ylabel('$\rho_{ess}$', 'Interpreter','latex', FontSize=16)