% Tabula rasa
clear;
close all;
clc;
% Define variables for size of A and sampling of parameters
number_of_agents = 100;
number_of_samples = 100;
    
x = linspace(-2, 2, number_of_samples);
y = linspace(-2, 2, number_of_samples);
z = linspace(-2, 2, number_of_samples);

% Define identity matrix and zero matrix of appropriate size, used often
identity = eye(number_of_agents);
zero = zeros(number_of_agents, number_of_agents);
% List of all tested improvements of the AAA

algorithms = ['1', '2', '3', '4', '5'];

% Initialize log-file to write result of comparisons to 
fid = fopen(fullfile(tempdir, 'comparison.txt'), 'a');
    if fid == -1
        error("Cannot open log file.");
    else
        fprintf(fid, "Epoch \t Alg. \t alpha \t beta \t gamma \t rho_ess\n");
    end
% Create new A's randomly and test all algorithms on the same matrix
for epoch=1:10
    A = create_A_matrix(number_of_agents);
    % Loop over algorithm, find optimal parameters, calculate rho_ess
    for alg=1:5
        
        algorithm = algorithms(alg);
        fprintf("Epoch %d, Algorithm %s\n", epoch, algorithm);
    
        alphas = zeros(number_of_samples^2, 1);
        betas = zeros(number_of_samples^2, 1);
        gammas = zeros(number_of_samples^2, 1);
        rhos = zeros(number_of_samples^2, 1);
        
        % Initialize counter for storing values in arrays defined above
        counter = 1;
        if ismember(algorithm, ['1', '2', '3', '5'])
    
            for i=1:length(x)
                beta = x(i);
                for j=1:length(y)
                    gamma = y(j);
                    
                    % Calculate T based on algorithm
                    switch algorithm
    
                        case '1'
                        T = [(beta-gamma)*A (1-beta)*identity gamma*identity; identity zero zero; zero identity zero];
                    
                        case '2'
                        T = [beta*A (1-beta)*identity gamma*identity; identity zero zero; zero identity zero];
    
                        case '3'
                        T = [beta*A (1-beta-gamma)*identity gamma*identity; identity zero zero; zero identity zero];
    
                        case '5'
                        T = [beta*A (gamma-beta)*identity (1-gamma)*identity; identity zero zero; zero identity zero];
                    end
                    % Largest eigenvalue magnitude that does not equal one
                    rho_ess = spectral_radius(eig(T));
                    
                    % Append values
                    betas(counter) = beta;
                    gammas(counter) = gamma;
                    rhos(counter) = rho_ess;
                    counter = counter + 1;
                end
            end
        elseif algorithm == '4'
            % For algorithm 4, the sum of all parameters has to be one
            for i=1:length(x)
                alpha = x(i);
                for j=1:length(y)
                    beta = y(j);
                    gamma = 1-alpha-beta;
    
                    T = [alpha*A beta*identity gamma*identity; identity zero zero; zero identity zero];
                    rho_ess = spectral_radius(eig(T));
    
                    alphas(counter) = alpha;
                    betas(counter) = beta;
                    gammas(counter) = gamma;
                    rhos(counter) = rho_ess;
                    counter = counter + 1;
                end
            end
        end
        % Find smallest possible rho_ess, get index to find optimal parameters
        [rho_ess_opt, idx] = min(rhos);
        fid = fopen(fullfile(tempdir, 'comparison.txt'), 'a');
        if fid == -1
            error("Cannot open log file.");
        end
        if algorithm == '4'
            % Write epoch, algorithm number, parameter values and rho_ess
            % to log file
            fprintf(fid, "%d \t %s \t %f \t %f \t %f \t %f\n", epoch, algorithm, alphas(idx), betas(idx), gammas(idx), rhos(idx));
        else
            fprintf(fid, "%d \t %s \t %f \t %f \t %f \t %f\n", epoch, algorithm, 0, betas(idx), gammas(idx), rhos(idx));            
        end
        fclose(fid);
    end
    rho_ess_A = spectral_radius(eig(A));
    fid = fopen(fullfile(tempdir, 'comparison.txt'), 'a');
    if fid == -1
        error("Cannot open log file.");
    else
        fprintf(fid, "%d \t Avg \t %f \t %f \t %f \t %f\n", epoch, 0, 0 , 0, rho_ess_A);
        fprintf(fid, "%d \t AAA \t %f \t %f \t %f \t %f\n", epoch, 0, 2/(1+sqrt(1-rho_ess_A^2)), 0, rho_ess_A/(1+sqrt(1-rho_ess_A^2)));
    end
    fclose(fid);
end