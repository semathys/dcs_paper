import numpy as np
from tqdm import tqdm

number_of_agents = 100
diagonal = 1/3*np.ones((number_of_agents,))
A = np.diag(diagonal)

for i in range(number_of_agents):
    if i<number_of_agents:
        A[i-1, i] = 1/3
    if i>0:
        A[i, i-1] = 1/3


A[number_of_agents-1, 0] = A[0, number_of_agents-1] = 1/3

x = np.linspace(-5, 5, 1000)
y = np.linspace(-5, 5, 1000)

def check_conditions(beta: float, gamma: float) -> bool:
    
    # Condition for real-valued beta and gamma. Check first to avoid having to deal with negative values in square root later on
    if not (beta**2-2*beta*gamma-2*gamma-2*beta+gamma**2+1 >= 0):
        return False

    # Conditions for eigenvalue of negative branch
    if not ((beta-gamma-1-np.sqrt(beta**2 -2*beta*gamma-2*beta-2*gamma+gamma**2+1))/2 > -1):
        return False
    if not ((beta-gamma-1-np.sqrt(beta**2 -2*beta*gamma-2*beta-2*gamma+gamma**2+1))/2 < 1):
        return False
    
    # Conditions for eigenvalue of positive branch
    if not ((beta-gamma-1+np.sqrt(beta**2 -2*beta*gamma-2*beta-2*gamma+gamma**2+1))/2 > -1):
        return False
    if not ((beta-gamma-1+np.sqrt(beta**2 -2*beta*gamma-2*beta-2*gamma+gamma**2+1))/2 < 1):
        return False

    
    return True

identity = np.identity(number_of_agents)
zero = np.zeros((number_of_agents, number_of_agents))



betas = []
gammas = []
rhos = []
for beta in tqdm(x):
    for gamma in y:
        if check_conditions(beta, gamma):
            # If conditions are satisfied, calculate iteration matrix T and derive its essential spectral radius
            T = np.block([[(beta-gamma)*A, (1-beta)*identity, gamma*identity], [identity, zero, zero], [zero, identity, zero]])
            # Calculate eigenvalues of T
            eigenvalues, _ = np.linalg.eig(T)
            # Eigenvalues are not necessarily sorted -> sort, then extract second to last entry for second largest eigenvalue
            rho_ess = sorted(np.abs(eigenvalues))[-2]
            betas.append(beta)
            gammas.append(gamma)
            rhos.append(rho_ess)

# Get index of smallest rho_ess
opt_index = rhos.index(min(rhos))
print(f"Optimal values: beta={betas[opt_index]}, gamma={gammas[opt_index]}")
print(f"This yields rho={min(rhos)}")