from tqdm import tqdm
import numpy as np

number_of_agents = 100
diagonal = 1/3*np.ones((number_of_agents,))
A = np.diag(diagonal)

for i in range(number_of_agents):
    if i<number_of_agents:
        A[i-1, i] = 1/3
    if i>0:
        A[i, i-1] = 1/3


A[number_of_agents-1, 0] = A[0, number_of_agents-1] = 1/3

x = np.linspace(-2, 2, 1000)

identity = np.identity(number_of_agents)
zero = np.zeros((number_of_agents, number_of_agents))

betas = []
gammas = []
rhos = []
for beta in tqdm(x):
    # Enforce beta+gamma=1 such that T has eigenvector of ones
    # On second thought, makes no sense. In this case, the only gamma that enforces this is gamma=0, which recovers AAA
    gamma = 0
    T = np.block([[beta*A, (1-beta)*identity, gamma*identity], [identity, zero, zero], [zero, identity, zero]])
    # Calculate eigenvalues of T
    eigenvalues, _ = np.linalg.eig(T)
    # Eigenvalues are not necessarily sorted -> sort, then extract second to last entry for second largest eigenvalue
    rho_ess = sorted(np.abs(eigenvalues))[-2]
    
    betas.append(beta)
    gammas.append(gamma)
    rhos.append(rho_ess)

# Get index of smallest rho_ess
opt_index = rhos.index(min(rhos))
print(f"Optimal values: beta={betas[opt_index]}, gamma={gammas[opt_index]}")
print(f"This yields rho={min(rhos)}")