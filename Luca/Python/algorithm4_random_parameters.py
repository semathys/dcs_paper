import numpy as np
import random
from tqdm import tqdm

number_of_agents = 100
diagonal = 1/3*np.ones((number_of_agents,))
A = np.diag(diagonal)

for i in range(number_of_agents):
    if i<number_of_agents:
        A[i-1, i] = 1/3
    if i>0:
        A[i, i-1] = 1/3

alphas = []
betas = []
gammas = []
rhos = []

identity = np.identity(number_of_agents)
zero = np.zeros((number_of_agents, number_of_agents))

A[number_of_agents-1, 0] = A[0, number_of_agents-1] = 1/3
for _ in tqdm(range(10000)):
    sequence = [x for x in np.arange(-5, 5, 0.005)]
    numbers = sorted(random.sample(sequence, 2))
    alpha = numbers[0]
    beta = numbers[1]-numbers[0]
    gamma = 1-numbers[1]

    T = np.block([[alpha*A, beta*identity, gamma*identity], [identity, zero, zero], [zero, identity, zero]])
    # Calculate eigenvalues of T
    eigenvalues, _ = np.linalg.eig(T)
    # Eigenvalues are not necessarily sorted -> sort, then extract second to last entry for second largest eigenvalue
    rho_ess = sorted(np.abs(eigenvalues))[-2]
    alphas.append(alpha)
    betas.append(beta)
    gammas.append(gamma)
    rhos.append(rho_ess)

opt_index = rhos.index(min(rhos))
print(f"Optimal values: alpha={alphas[opt_index]}, beta={betas[opt_index]}, gamma={gammas[opt_index]}")
print(f"This yields rho={min(rhos)}")